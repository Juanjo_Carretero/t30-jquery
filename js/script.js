var primerValor = 0;
var operador = "";
var segundoValor = "";
var resultado = 0;
var porTeclado = false;


$(function(){

    $(".pulsarBoton").on("click",
    function(event){
        var id = this.id;
        agregarBoton(id);
        
    });


    $("#botonRetr").on("click",
    function(){
        retroceder();
    })

    $("#botonCE").on("click",
    function(){
        ultimaEntrada();
    })


    $("#botonC").on("click",
    function(){
        borrarTodo();
    });

    $("#raiz").on("click",
    function(){
        raiz();
    })

    $("#dividir").on("click",
    function(){
        dividir();
    })

    $("#multiplicar").on("click",
    function(){
        multiplicar();
    })

    $("#porcentaje").on("click",
    function(){
        porcentaje();
    })

    $("#restar").on("click",
    function(){
        restar();
    })
    
    $("#unoEntreX").on("click",
    function(){
        entreUno();
    })

    $("#MasMenos").on("click",
    function(){
        cambiarSigno();
    })

    $("#punto").on("click",
    function(){
        punto();
    })

    $("#sumar").on("click",
    function(){
        sumar();
    })

    $("#igual").on("click",
    function(){
        calcular();
    })
 
    $("#campoTexto").on("keydown",
    function(){
        botones();
    })
  
});

    function agregarBoton(id){
    var texto = $("#campoTexto");   
    texto.val(texto.val() + id);
    }

    function punto(){
    var texto = $("#campoTexto");   
    texto.val(texto.val() + ".");
    }

    function sumar(){
    var numero = $("#campoTexto");
    primerValor = numero.val();
    operador = "+";
    if(!porTeclado){
    numero.val(numero.val() + operador);
    }
    }

    function restar(){
    var numero = $("#campoTexto");
    primerValor = numero.val();
    operador = "-";
    if(!porTeclado){
    numero.val(numero.val() + operador);
    } 
    }

    function dividir(){
    var numero = $("#campoTexto");
    primerValor = numero.val();
    operador = "/";
    if(!porTeclado){
    numero.val(numero.val() + operador);
    } 
    }

    function multiplicar(){
    var numero = $("#campoTexto");
    primerValor = numero.val();
    operador = "*";
    if(!porTeclado){
    numero.val(numero.val() + operador);
    } 
    }

    function porcentaje(){
    var numero = $("#campoTexto");
    primerValor = numero.val();
    operador = "%";
    if(!porTeclado){
    numero.val(numero.val() + operador);
    } 
    }

    function raiz(){
    var numero = $("#campoTexto");
    var resultado = Math.sqrt(numero.val())
    numero.val(resultado);
    }

    function entreUno(){
    var numero = $("#campoTexto");
    var resultado = 1 / numero.val()
    numero.val(resultado);
    }

    function obtenerSegundoValor(){  
    var texto = $("#campoTexto").val()

    if(texto.indexOf(operador)){
    for (var index = texto.indexOf(operador)+1; index < texto.length; index++) {       
        segundoValor = segundoValor + texto[index];
        console.log(segundoValor)
        
    }
    return segundoValor;
    }




    }

    function calcular(){
    var segundoValor = obtenerSegundoValor();
    switch (operador) {
    case "+":
        resultado = parseFloat(primerValor)+parseFloat(segundoValor);
        $("#campoTexto").val(resultado);
        resetearVariables()
        break;
    case "-":
        resultado = parseFloat(primerValor)-parseFloat(segundoValor);
        $("#campoTexto").val(resultado);           
        resetearVariables()

        break;
    case "*":
        resultado = parseFloat(primerValor)*parseFloat(segundoValor);
        $("#campoTexto").val(resultado);
        resetearVariables()

        break;
    case "/":
        resultado = parseFloat(primerValor)/parseFloat(segundoValor);
        $("#campoTexto").val(resultado);           
        resetearVariables()

        break;
    case "%":
        resultado = parseFloat(primerValor)%parseFloat(segundoValor);
        $("#campoTexto").val(resultado);           
        resetearVariables()

        break;

    default:
        break;
    }


    }

    function resetearVariables(){
    primerValor = 0;
    operador = "";
    segundoValor = "";
    resultado = 0;
    porTeclado = false;
    }

    function cambiarSigno(){
    var texto = $("#campoTexto").val()
    var nuevaCadena = ""
    if(!texto.indexOf(operador)){

            for (var index = 0; index < texto.length; index++) {       
                if(texto[0] == "-"){                                       
                    $("#campoTexto").val(texto.replace('-',''));
                }else{
                    nuevaCadena = "-" + texto[index];
                    $("#campoTexto").val(nuevaCadena);
                }
            
            }

    }else{
        var textoAntesOperador = antesOperador();        
        var textoDespuesOperador = despuesOperador();

        
            if(texto[textoAntesOperador.length] == "-"){ 
                $("#campoTexto").val(texto.replace('-',''))
                nuevaCadena = $("#campoTexto").val()
            }else{
                nuevaCadena = textoAntesOperador+"-"+textoDespuesOperador;
                $("#campoTexto").val(nuevaCadena);

            }
    }

    }

    function antesOperador(){
        var texto = $("#campoTexto").val()

        var textoAntesOperador = "";

        for (var index = 0; index < texto.indexOf(operador)+1; index++) {
            textoAntesOperador = textoAntesOperador + texto[index];

        }

        return textoAntesOperador;
    }

    function despuesOperador(){
        var texto = $("#campoTexto").val()

        var textoAntesOperador = antesOperador();
        var textoDespuesOperador = "";

        for (var index = textoAntesOperador.length; index < texto.length; index++) {
            textoDespuesOperador += texto[index];               
        }
        return textoDespuesOperador;
    }

    function borrarTodo(){
        resetearVariables()
        $("#campoTexto").val("");
    }

    function ultimaEntrada(){
        var texto = $("#campoTexto").val()
    
        if(!texto.indexOf(operador)){
            borrarTodo();
        }else{
            var nuevaCadena = "";
            for (var index = 0; index < texto.indexOf(operador)+1; index++) {       
                nuevaCadena = nuevaCadena + texto[index];
            }
            $("#campoTexto").val(nuevaCadena);
        }
    
    }

    function retroceder(){
        var texto = $("#campoTexto").val()
    
        if(texto.length <= 1){
            $("#campoTexto").val("");
              
        }else{             
            var nuevaCadena = ""           
            for (var index = 0; index < texto.length-1; index++) {
                nuevaCadena += texto[index];
                
            }
            $("#campoTexto").val(nuevaCadena);
        }
    
    }

    function botones () {
        var codigo = event.which || event.keyCode;
        console.log("Presionada: " + codigo);
        porTeclado = true; 
        switch (codigo) {
            case 107:
                sumar();
                break;
            case 109:
                restar();
                break;
            case 106:
                multiplicar();
                break;
            case 111:
                dividir();
                break;
            case 53:
                porcentaje();
                break;
            case 48:
                calcular();
                break;
            case 13:
                calcular();
                break;
        
            default:
                break;
        }
        
    
        
    }

